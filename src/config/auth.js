import JsonWebToken from "jsonwebtoken";
import User from "../models/User";

export default class Auth {
    static auth(roles){
        return async(req, res, next) => {
            try {
                let token = req.headers.authorization.replace(/Bearer /g, '');
                let decryptToken = JsonWebToken.decode(token, process.env.AUTHKEY);
                let user = await User.findById(decryptToken.sub);

                if(user && user.user_role === 10){
                    next();
                }else if(roles.includes(user.user_role)){
                    next();
                }else{
                    res.status(401).json({'message' : 'Unauthorize'});
                }

            } catch (error) {
                res.status(403).json({'message': 'Error'})
            }
            
        }
    }
}