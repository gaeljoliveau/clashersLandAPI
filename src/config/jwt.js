import expressJWt from 'express-jwt';
import User from '../models/User';

function jwt(){
    //A random string
    const secret = process.env.AUTHKEY;
    return expressJWt({secret, isRevoked}).unless({
        path: [
            '/users/autenticate'
        ]
    });
}

async function isRevoked(req, payload, done){
    const user = User.findById(payload.sub);
    if(!user){
        return done(null, true);
    }
    done();
}

export default jwt;