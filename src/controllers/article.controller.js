import Article from '../models/Article';

export default class ArticleController {
/**
 * 
 * @param {request} req 
 * @param {response} res 
 */

    static async create(req, res){
        let status = 200;
        let body = {};

        try {
            let newArticle = await Article.create({
                title: req.body.title,
                content: req.body.content,
                date: new Date
            });
            body = {
                newArticle,
                'message': 'Article created'
            }
        } catch (error) {
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        console.log("test update article");

        try {
            let article = await Article.findOneAndUpdate({_id : req.params.id},
                {
                    title: req.body.title,
                    content: req.body.content
                },
                {new: true}
                );

            body = {
                article,
                'message': 'Article updated'
            }
        } catch (error) {
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let articles = await Article.aggregate([
                {
                    $project: {
                        _id: '$_id',
                        date:'$date', 
                        title:'$title', 
                        content:{$substr: [ "$content", 0, 5000 ] }}
                },
                {
                    $sort: {'date': -1}
                }]);

            body = {articles, 'message': 'articles list'}
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        

        return res.status(status).json(body);
    }

    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let article = await Article.findById(req.params.id);
                

            body = {article, 'message': 'article asked'}
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    static async delete(req, res){

        let status = 200;
        let body = {};

        try{
            console.log(req.params.id);
            await Article.remove({_id: req.params.id});

            body = {'message': 'Users deleted'}
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        

        return res.status(status).json(body);
    }
}