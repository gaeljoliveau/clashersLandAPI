import SubjectCategory from "../models/SubjectCategory";

export default class SubjectCategoryController{

    /**
     * Creates a subjectCategory in a database
     * @param {Request} req 
     * @param {Response} res 
     */

    static async create (req, res) {
        let status = 200;
        let body = {};

        try{
            console.log("req.body : " + req.body);
            let newSubjectCategory = await SubjectCategory.create({
                name: req.body.name,
                description: req.body.description,
            });

            body = {
                newSubjectCategory,
                'message': 'User created'
            }
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    static async delete (req, res){
        let status = 200;
        let body = {};

        try {
            await SubjectCategory.remove({_id: req.params.id});
            body = {'message': 'Category deleted'}
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    static async update (req, res){
        let status = 200;
        let body = {};

        try {
            let cat = await SubjectCategory.findByIdAndUpdate({_id: req.params.id},
                {
                    name: req.body.name,
                    description: req.body.description
                },
                {new: true});
            body = {'category': cat,'message': 'Category updated'}
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    static async list (req, res){
        let status = 200;
        let body = {};
        console.log("asking for subject categories");
        try {
            let categories = await SubjectCategory.find().select('-_v');
            body = {
                categories,
                message: "categories list" 
            };
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }
}