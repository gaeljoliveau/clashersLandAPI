import mongoose from 'mongoose';

const ArticleSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    date: Date,
    content: String,
    image: { data: Buffer, contentType: String }
});

const Article = mongoose.model('Article', ArticleSchema);
export default Article;