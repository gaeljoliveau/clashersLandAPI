import mongoose from 'mongoose';

const subjectCategorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: String,
    image: { data: Buffer, contentType: String }
});

const SubjectCategory = mongoose.model('SubjectCategory', subjectCategorySchema);
export default SubjectCategory;