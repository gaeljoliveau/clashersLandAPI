import mongoose from 'mongoose';

const subjectSchema = new mongoose.Schema({
    userId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    categoryId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'SubjectCategory'
    },
    date: {
        type: Date,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    comments: 
        [{
            userId:{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            date: {
                type: Date,
                required: true
            },
            comment : String
        }]
});


const Subject = mongoose.model('Subject', subjectSchema);
export default Subject;