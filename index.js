import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import router from './src/routes/routes';
import database from './src/models/database';



//Init
const app = express();
require('dotenv').config();

// JWT 
//app.use(jwt());

//Config
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(cors({origin:true}));

//Use route
app.use(router);

//Launch
const port = 3001;

database.connectDb().then(() => {
    console.log('Database server is connected !');
    app.listen(port, () => {
        console.log(`Server listening on port ${port}`)
    });
})



